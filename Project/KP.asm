
_interrupt:

;KP.c,6 :: 		void interrupt() {
;KP.c,7 :: 		USB_Interrupt_Proc();
	CALL        _USB_Interrupt_Proc+0, 0
;KP.c,8 :: 		TMR0L = 100;       //Reload Value
	MOVLW       100
	MOVWF       TMR0L+0 
;KP.c,9 :: 		INTCON.TMR0IF = 0;    //Re-Enable Timer-0 Interrupt
	BCF         INTCON+0, 2 
;KP.c,10 :: 		}
L_end_interrupt:
L__interrupt23:
	RETFIE      1
; end of _interrupt

_UART1_Write_Text_Newline:

;KP.c,36 :: 		void UART1_Write_Text_Newline(unsigned char msg[]) {
;KP.c,37 :: 		UART1_Write_Text(msg);
	MOVF        FARG_UART1_Write_Text_Newline_msg+0, 0 
	MOVWF       FARG_UART1_Write_Text_uart_text+0 
	MOVF        FARG_UART1_Write_Text_Newline_msg+1, 0 
	MOVWF       FARG_UART1_Write_Text_uart_text+1 
	CALL        _UART1_Write_Text+0, 0
;KP.c,38 :: 		UART1_Write(10);
	MOVLW       10
	MOVWF       FARG_UART1_Write_data_+0 
	CALL        _UART1_Write+0, 0
;KP.c,39 :: 		UART1_Write(13);
	MOVLW       13
	MOVWF       FARG_UART1_Write_data_+0 
	CALL        _UART1_Write+0, 0
;KP.c,40 :: 		}
L_end_UART1_Write_Text_Newline:
	RETURN      0
; end of _UART1_Write_Text_Newline

_clear_buffer:

;KP.c,41 :: 		void clear_buffer(unsigned char buffer[]) {
;KP.c,42 :: 		unsigned int i = 0;
	CLRF        clear_buffer_i_L0+0 
	CLRF        clear_buffer_i_L0+1 
;KP.c,43 :: 		while (buffer[i] != '\0') {
L_clear_buffer0:
	MOVF        clear_buffer_i_L0+0, 0 
	ADDWF       FARG_clear_buffer_buffer+0, 0 
	MOVWF       FSR0L 
	MOVF        clear_buffer_i_L0+1, 0 
	ADDWFC      FARG_clear_buffer_buffer+1, 0 
	MOVWF       FSR0H 
	MOVF        POSTINC0+0, 0 
	XORLW       0
	BTFSC       STATUS+0, 2 
	GOTO        L_clear_buffer1
;KP.c,44 :: 		buffer[i] = '\0';
	MOVF        clear_buffer_i_L0+0, 0 
	ADDWF       FARG_clear_buffer_buffer+0, 0 
	MOVWF       FSR1L 
	MOVF        clear_buffer_i_L0+1, 0 
	ADDWFC      FARG_clear_buffer_buffer+1, 0 
	MOVWF       FSR1H 
	CLRF        POSTINC1+0 
;KP.c,45 :: 		i++;
	INFSNZ      clear_buffer_i_L0+0, 1 
	INCF        clear_buffer_i_L0+1, 1 
;KP.c,46 :: 		}
	GOTO        L_clear_buffer0
L_clear_buffer1:
;KP.c,47 :: 		}
L_end_clear_buffer:
	RETURN      0
; end of _clear_buffer

_main:

;KP.c,49 :: 		void main() {
;KP.c,50 :: 		UART1_Init(4800);
	MOVLW       155
	MOVWF       SPBRG+0 
	BCF         TXSTA+0, 2, 0
	CALL        _UART1_Init+0, 0
;KP.c,51 :: 		Delay_ms(100);
	MOVLW       7
	MOVWF       R11, 0
	MOVLW       23
	MOVWF       R12, 0
	MOVLW       106
	MOVWF       R13, 0
L_main2:
	DECFSZ      R13, 1, 1
	BRA         L_main2
	DECFSZ      R12, 1, 1
	BRA         L_main2
	DECFSZ      R11, 1, 1
	BRA         L_main2
	NOP
;KP.c,52 :: 		UART1_Write_Text("USB Test Program");
	MOVLW       ?lstr1_KP+0
	MOVWF       FARG_UART1_Write_Text_uart_text+0 
	MOVLW       hi_addr(?lstr1_KP+0)
	MOVWF       FARG_UART1_Write_Text_uart_text+1 
	CALL        _UART1_Write_Text+0, 0
;KP.c,53 :: 		ADCON1 |= 0x0F;          // Configure AN pins as digital
	MOVLW       15
	IORWF       ADCON1+0, 1 
;KP.c,54 :: 		CMCON |= 7;            // Disable comparators
	MOVLW       7
	IORWF       CMCON+0, 1 
;KP.c,55 :: 		TRISB = 0x00;
	CLRF        TRISB+0 
;KP.c,56 :: 		TRISC = 0x80;
	MOVLW       128
	MOVWF       TRISC+0 
;KP.c,57 :: 		Lcd_Init();            // Initialize Lcd
	CALL        _Lcd_Init+0, 0
;KP.c,58 :: 		Delay_ms(100);
	MOVLW       7
	MOVWF       R11, 0
	MOVLW       23
	MOVWF       R12, 0
	MOVLW       106
	MOVWF       R13, 0
L_main3:
	DECFSZ      R13, 1, 1
	BRA         L_main3
	DECFSZ      R12, 1, 1
	BRA         L_main3
	DECFSZ      R11, 1, 1
	BRA         L_main3
	NOP
;KP.c,59 :: 		Lcd_Cmd(_LCD_CLEAR);       // Clear display
	MOVLW       1
	MOVWF       FARG_Lcd_Cmd_out_char+0 
	CALL        _Lcd_Cmd+0, 0
;KP.c,60 :: 		Delay_ms(100);
	MOVLW       7
	MOVWF       R11, 0
	MOVLW       23
	MOVWF       R12, 0
	MOVLW       106
	MOVWF       R13, 0
L_main4:
	DECFSZ      R13, 1, 1
	BRA         L_main4
	DECFSZ      R12, 1, 1
	BRA         L_main4
	DECFSZ      R11, 1, 1
	BRA         L_main4
	NOP
;KP.c,61 :: 		Lcd_Cmd(_LCD_CURSOR_OFF);     // Cursor off
	MOVLW       12
	MOVWF       FARG_Lcd_Cmd_out_char+0 
	CALL        _Lcd_Cmd+0, 0
;KP.c,62 :: 		Delay_ms(100);
	MOVLW       7
	MOVWF       R11, 0
	MOVLW       23
	MOVWF       R12, 0
	MOVLW       106
	MOVWF       R13, 0
L_main5:
	DECFSZ      R13, 1, 1
	BRA         L_main5
	DECFSZ      R12, 1, 1
	BRA         L_main5
	DECFSZ      R11, 1, 1
	BRA         L_main5
	NOP
;KP.c,63 :: 		Lcd_Out(1, 3, "PIC18F4550");        // Write text in first row
	MOVLW       1
	MOVWF       FARG_Lcd_Out_row+0 
	MOVLW       3
	MOVWF       FARG_Lcd_Out_column+0 
	MOVLW       ?lstr2_KP+0
	MOVWF       FARG_Lcd_Out_text+0 
	MOVLW       hi_addr(?lstr2_KP+0)
	MOVWF       FARG_Lcd_Out_text+1 
	CALL        _Lcd_Out+0, 0
;KP.c,64 :: 		Delay_ms(100);
	MOVLW       7
	MOVWF       R11, 0
	MOVLW       23
	MOVWF       R12, 0
	MOVLW       106
	MOVWF       R13, 0
L_main6:
	DECFSZ      R13, 1, 1
	BRA         L_main6
	DECFSZ      R12, 1, 1
	BRA         L_main6
	DECFSZ      R11, 1, 1
	BRA         L_main6
	NOP
;KP.c,65 :: 		Lcd_Out(2, 3, "USB Example!");        // Write text in second row
	MOVLW       2
	MOVWF       FARG_Lcd_Out_row+0 
	MOVLW       3
	MOVWF       FARG_Lcd_Out_column+0 
	MOVLW       ?lstr3_KP+0
	MOVWF       FARG_Lcd_Out_text+0 
	MOVLW       hi_addr(?lstr3_KP+0)
	MOVWF       FARG_Lcd_Out_text+1 
	CALL        _Lcd_Out+0, 0
;KP.c,66 :: 		Delay_ms(200);
	MOVLW       13
	MOVWF       R11, 0
	MOVLW       45
	MOVWF       R12, 0
	MOVLW       215
	MOVWF       R13, 0
L_main7:
	DECFSZ      R13, 1, 1
	BRA         L_main7
	DECFSZ      R12, 1, 1
	BRA         L_main7
	DECFSZ      R11, 1, 1
	BRA         L_main7
	NOP
	NOP
;KP.c,67 :: 		INTCON = 0;
	CLRF        INTCON+0 
;KP.c,68 :: 		INTCON2 = 0xF5;
	MOVLW       245
	MOVWF       INTCON2+0 
;KP.c,69 :: 		INTCON3 = 0xC0;
	MOVLW       192
	MOVWF       INTCON3+0 
;KP.c,70 :: 		RCON.IPEN = 0;
	BCF         RCON+0, 7 
;KP.c,71 :: 		PIE1 = 0;
	CLRF        PIE1+0 
;KP.c,72 :: 		PIE2 = 0;
	CLRF        PIE2+0 
;KP.c,73 :: 		PIR1 = 0;
	CLRF        PIR1+0 
;KP.c,74 :: 		PIR2 = 0;
	CLRF        PIR2+0 
;KP.c,81 :: 		T0CON = 0x47; // Prescaler = 256
	MOVLW       71
	MOVWF       T0CON+0 
;KP.c,82 :: 		TMR0L = 100; // Timer count is 256-156 = 100
	MOVLW       100
	MOVWF       TMR0L+0 
;KP.c,83 :: 		INTCON.TMR0IE = 1; // Enable T0IE
	BSF         INTCON+0, 5 
;KP.c,84 :: 		T0CON.TMR0ON = 1; // Turn Timer 0 ON
	BSF         T0CON+0, 7 
;KP.c,85 :: 		INTCON = 0xE0; // Enable interrupts
	MOVLW       224
	MOVWF       INTCON+0 
;KP.c,89 :: 		UART1_Write(10);
	MOVLW       10
	MOVWF       FARG_UART1_Write_data_+0 
	CALL        _UART1_Write+0, 0
;KP.c,90 :: 		UART1_Write(13);
	MOVLW       13
	MOVWF       FARG_UART1_Write_data_+0 
	CALL        _UART1_Write+0, 0
;KP.c,91 :: 		UART1_Write_Text_Newline("Data is Ready to be Received from the PC");
	MOVLW       ?lstr4_KP+0
	MOVWF       FARG_UART1_Write_Text_Newline_msg+0 
	MOVLW       hi_addr(?lstr4_KP+0)
	MOVWF       FARG_UART1_Write_Text_Newline_msg+1 
	CALL        _UART1_Write_Text_Newline+0, 0
;KP.c,92 :: 		Hid_Enable(&Read_Buffer, &Write_Buffer);
	MOVLW       _Read_Buffer+0
	MOVWF       FARG_HID_Enable_readbuff+0 
	MOVLW       hi_addr(_Read_Buffer+0)
	MOVWF       FARG_HID_Enable_readbuff+1 
	MOVLW       _Write_Buffer+0
	MOVWF       FARG_HID_Enable_writebuff+0 
	MOVLW       hi_addr(_Write_Buffer+0)
	MOVWF       FARG_HID_Enable_writebuff+1 
	CALL        _HID_Enable+0, 0
;KP.c,93 :: 		Delay_ms(200);
	MOVLW       13
	MOVWF       R11, 0
	MOVLW       45
	MOVWF       R12, 0
	MOVLW       215
	MOVWF       R13, 0
L_main8:
	DECFSZ      R13, 1, 1
	BRA         L_main8
	DECFSZ      R12, 1, 1
	BRA         L_main8
	DECFSZ      R11, 1, 1
	BRA         L_main8
	NOP
	NOP
;KP.c,95 :: 		start:
___main_start:
;KP.c,96 :: 		while (Hid_Read() == 0);  //Stay Here if Data is Not Coming from Serial Port
L_main9:
	CALL        _HID_Read+0, 0
	MOVF        R0, 0 
	XORLW       0
	BTFSS       STATUS+0, 2 
	GOTO        L_main10
	GOTO        L_main9
L_main10:
;KP.c,98 :: 		if (Read_Buffer[0] == 0x81) {
	MOVF        1280, 0 
	XORLW       129
	BTFSS       STATUS+0, 2 
	GOTO        L_main11
;KP.c,99 :: 		Lcd_Cmd(_LCD_CLEAR);
	MOVLW       1
	MOVWF       FARG_Lcd_Cmd_out_char+0 
	CALL        _Lcd_Cmd+0, 0
;KP.c,100 :: 		Lcd_Out(1, 2, "Authentication");
	MOVLW       1
	MOVWF       FARG_Lcd_Out_row+0 
	MOVLW       2
	MOVWF       FARG_Lcd_Out_column+0 
	MOVLW       ?lstr5_KP+0
	MOVWF       FARG_Lcd_Out_text+0 
	MOVLW       hi_addr(?lstr5_KP+0)
	MOVWF       FARG_Lcd_Out_text+1 
	CALL        _Lcd_Out+0, 0
;KP.c,101 :: 		Lcd_Out(2, 8, "OK");
	MOVLW       2
	MOVWF       FARG_Lcd_Out_row+0 
	MOVLW       8
	MOVWF       FARG_Lcd_Out_column+0 
	MOVLW       ?lstr6_KP+0
	MOVWF       FARG_Lcd_Out_text+0 
	MOVLW       hi_addr(?lstr6_KP+0)
	MOVWF       FARG_Lcd_Out_text+1 
	CALL        _Lcd_Out+0, 0
;KP.c,102 :: 		while (!HID_Write(&recv, 64));
L_main12:
	MOVLW       _recv+0
	MOVWF       FARG_HID_Write_writebuff+0 
	MOVLW       hi_addr(_recv+0)
	MOVWF       FARG_HID_Write_writebuff+1 
	MOVLW       64
	MOVWF       FARG_HID_Write_len+0 
	CALL        _HID_Write+0, 0
	MOVF        R0, 1 
	BTFSS       STATUS+0, 2 
	GOTO        L_main13
	GOTO        L_main12
L_main13:
;KP.c,103 :: 		}
	GOTO        L_main14
L_main11:
;KP.c,105 :: 		Lcd_Cmd(_LCD_CLEAR);
	MOVLW       1
	MOVWF       FARG_Lcd_Cmd_out_char+0 
	CALL        _Lcd_Cmd+0, 0
;KP.c,106 :: 		Lcd_Out(1, 2, "Authentication");
	MOVLW       1
	MOVWF       FARG_Lcd_Out_row+0 
	MOVLW       2
	MOVWF       FARG_Lcd_Out_column+0 
	MOVLW       ?lstr7_KP+0
	MOVWF       FARG_Lcd_Out_text+0 
	MOVLW       hi_addr(?lstr7_KP+0)
	MOVWF       FARG_Lcd_Out_text+1 
	CALL        _Lcd_Out+0, 0
;KP.c,107 :: 		Lcd_Out(2, 5, "Fails!");
	MOVLW       2
	MOVWF       FARG_Lcd_Out_row+0 
	MOVLW       5
	MOVWF       FARG_Lcd_Out_column+0 
	MOVLW       ?lstr8_KP+0
	MOVWF       FARG_Lcd_Out_text+0 
	MOVLW       hi_addr(?lstr8_KP+0)
	MOVWF       FARG_Lcd_Out_text+1 
	CALL        _Lcd_Out+0, 0
;KP.c,108 :: 		goto start;
	GOTO        ___main_start
;KP.c,109 :: 		}
L_main14:
;KP.c,113 :: 		loop_second:
___main_loop_second:
;KP.c,114 :: 		clear_buffer(recv);
	MOVLW       _recv+0
	MOVWF       FARG_clear_buffer_buffer+0 
	MOVLW       hi_addr(_recv+0)
	MOVWF       FARG_clear_buffer_buffer+1 
	CALL        _clear_buffer+0, 0
;KP.c,115 :: 		clear_buffer(Read_Buffer);
	MOVLW       _Read_Buffer+0
	MOVWF       FARG_clear_buffer_buffer+0 
	MOVLW       hi_addr(_Read_Buffer+0)
	MOVWF       FARG_clear_buffer_buffer+1 
	CALL        _clear_buffer+0, 0
;KP.c,117 :: 		while (1) {
L_main15:
;KP.c,118 :: 		if (UART_Data_Ready()) {
	CALL        _UART_Data_Ready+0, 0
	MOVF        R0, 1 
	BTFSC       STATUS+0, 2 
	GOTO        L_main17
;KP.c,119 :: 		recv[dataPointer + 1] = UART_Read();
	MOVLW       1
	ADDWF       _dataPointer+0, 0 
	MOVWF       R0 
	MOVLW       0
	ADDWFC      _dataPointer+1, 0 
	MOVWF       R1 
	MOVLW       _recv+0
	ADDWF       R0, 0 
	MOVWF       FLOC__main+0 
	MOVLW       hi_addr(_recv+0)
	ADDWFC      R1, 0 
	MOVWF       FLOC__main+1 
	CALL        _UART_Read+0, 0
	MOVFF       FLOC__main+0, FSR1L
	MOVFF       FLOC__main+1, FSR1H
	MOVF        R0, 0 
	MOVWF       POSTINC1+0 
;KP.c,120 :: 		dataPointer++;
	INFSNZ      _dataPointer+0, 1 
	INCF        _dataPointer+1, 1 
;KP.c,121 :: 		}
L_main17:
;KP.c,123 :: 		if (dataPointer > 62)
	MOVLW       128
	MOVWF       R0 
	MOVLW       128
	XORWF       _dataPointer+1, 0 
	SUBWF       R0, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__main27
	MOVF        _dataPointer+0, 0 
	SUBLW       62
L__main27:
	BTFSC       STATUS+0, 0 
	GOTO        L_main18
;KP.c,124 :: 		break;
	GOTO        L_main16
L_main18:
;KP.c,126 :: 		if (Hid_Read()) {
	CALL        _HID_Read+0, 0
	MOVF        R0, 1 
	BTFSC       STATUS+0, 2 
	GOTO        L_main19
;KP.c,127 :: 		recv[0] =  (unsigned char) dataPointer;
	MOVF        _dataPointer+0, 0 
	MOVWF       _recv+0 
;KP.c,128 :: 		while (!HID_Write(&recv, 64));
L_main20:
	MOVLW       _recv+0
	MOVWF       FARG_HID_Write_writebuff+0 
	MOVLW       hi_addr(_recv+0)
	MOVWF       FARG_HID_Write_writebuff+1 
	MOVLW       64
	MOVWF       FARG_HID_Write_len+0 
	CALL        _HID_Write+0, 0
	MOVF        R0, 1 
	BTFSS       STATUS+0, 2 
	GOTO        L_main21
	GOTO        L_main20
L_main21:
;KP.c,129 :: 		break;
	GOTO        L_main16
;KP.c,130 :: 		}
L_main19:
;KP.c,131 :: 		}
	GOTO        L_main15
L_main16:
;KP.c,132 :: 		dataPointer = 0;
	CLRF        _dataPointer+0 
	CLRF        _dataPointer+1 
;KP.c,133 :: 		Lcd_Cmd(_LCD_CLEAR);
	MOVLW       1
	MOVWF       FARG_Lcd_Cmd_out_char+0 
	CALL        _Lcd_Cmd+0, 0
;KP.c,134 :: 		Lcd_Out(1, 1, "Received Data:-");
	MOVLW       1
	MOVWF       FARG_Lcd_Out_row+0 
	MOVLW       1
	MOVWF       FARG_Lcd_Out_column+0 
	MOVLW       ?lstr9_KP+0
	MOVWF       FARG_Lcd_Out_text+0 
	MOVLW       hi_addr(?lstr9_KP+0)
	MOVWF       FARG_Lcd_Out_text+1 
	CALL        _Lcd_Out+0, 0
;KP.c,135 :: 		Lcd_Out(2, 1, recv);
	MOVLW       2
	MOVWF       FARG_Lcd_Out_row+0 
	MOVLW       1
	MOVWF       FARG_Lcd_Out_column+0 
	MOVLW       _recv+0
	MOVWF       FARG_Lcd_Out_text+0 
	MOVLW       hi_addr(_recv+0)
	MOVWF       FARG_Lcd_Out_text+1 
	CALL        _Lcd_Out+0, 0
;KP.c,137 :: 		goto loop_second;
	GOTO        ___main_loop_second
;KP.c,138 :: 		}
L_end_main:
	GOTO        $+0
; end of _main
