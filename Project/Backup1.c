unsigned char Read_Buffer[16] absolute 0x500;
unsigned char Write_Buffer[16]absolute 0x510;
unsigned char num, flag;
unsigned char writebuff[65] = "Komunikasi \r\n";
unsigned int data_counter = 0;
void interrupt() {
        USB_Interrupt_Proc();
        TMR0L = 100;       //Reload Value
        INTCON.TMR0IF = 0;    //Re-Enable Timer-0 Interrupt
}
//LCD 8-bit Mode Connection
sbit LCD_RS at RC1_bit;
sbit LCD_RW at RC0_bit;
sbit LCD_EN at RC2_bit;
sbit LCD_D7 at RD7_bit;
sbit LCD_D6 at RD6_bit;
sbit LCD_D5 at RD5_bit;
sbit LCD_D4 at RD4_bit;
sbit LCD_D3 at RD3_bit;
sbit LCD_D2 at RD2_bit;
sbit LCD_D1 at RD1_bit;
sbit LCD_D0 at RD0_bit;
sbit LCD_RS_Direction at TRISC1_bit;
sbit LCD_RW_Direction at TRISC0_bit;
sbit LCD_EN_Direction at TRISC2_bit;
sbit LCD_D7_Direction at TRISD7_bit;
sbit LCD_D6_Direction at TRISD6_bit;
sbit LCD_D5_Direction at TRISD5_bit;
sbit LCD_D4_Direction at TRISD4_bit;
sbit LCD_D3_Direction at TRISD3_bit;
sbit LCD_D2_Direction at TRISD2_bit;
sbit LCD_D1_Direction at TRISD1_bit;
sbit LCD_D0_Direction at TRISD0_bit;
// End LCD module connections
char i;               // Loop variable

void UART1_Write_Text_Newline(unsigned char msg[]) {
        UART1_Write_Text(msg);
        UART1_Write(10);
        UART1_Write(13);
}
void clear_buffer(unsigned char buffer[]) {
        unsigned int i = 0;
        while (buffer[i] != '\0') {
                buffer[i] = '\0';
                i++;
        }
}
//
void main() {
        UART1_Init(9600);
        Delay_ms(100);
        ADCON1 |= 0x0F;          // Configure AN pins as digital
        CMCON |= 7;            // Disable comparators
        TRISB = 0x00;
        TRISC = 0x80;
        Lcd_Init();            // Initialize LCD
        Delay_ms(100);
        Lcd_Cmd(_LCD_CLEAR);       // Clear display
        Delay_ms(100);
        Lcd_Cmd(_LCD_CURSOR_OFF);     // Cursor off
        Delay_ms(100);
        Lcd_Out(1, 1, "TUGAS");        // Write text in first row
        Delay_ms(100);
        Lcd_Out(2, 1, "Kerja Praktek");        // Write text in second row
        Delay_ms(2000);
        INTCON = 0;
        INTCON2 = 0xF5;
        INTCON3 = 0xC0;
        RCON.IPEN = 0;
        PIE1 = 0;
        PIE2 = 0;
        PIR1 = 0;
        PIR2 = 0;
        //
        // Configure TIMER 0 for 3.3ms interrupts. Set prescaler to 256
        // and load TMR0L to 100 so that the time interval for timer
        // interrupts at 48MHz is 256.(256-100).0.083 = 3.3ms
        //
        // The timer is in 8-bit mode by default
        T0CON = 0x47; // Prescaler = 256
        TMR0L = 100; // Timer count is 256-156 = 100
        INTCON.TMR0IE = 1; // Enable T0IE
        T0CON.TMR0ON = 1; // Turn Timer 0 ON
        INTCON = 0xE0; // Enable interrupts
                //
                // Enable USB port
                //
        Hid_Enable(&Read_Buffer, &Write_Buffer);
        Delay_ms(200);

        // Read from the USB port. Number of bytes read is in num
start:
        while (Hid_Read() == 0);  //Stay Here if Data is Not Coming from Serial Port

        //If Some Data is Coming then move forward and check whether the keyword start is coming or not
        if (strncmp(Read_Buffer, "S", 1) == 0) {

                Lcd_Cmd(_LCD_CLEAR);
                Lcd_Out(1, 2, "Authentication");
                Lcd_Out(2, 8, "OK");
                while (!HID_Write(&writebuff, 64));
                goto loop;
        }
        else {
                Lcd_Cmd(_LCD_CLEAR);
                Lcd_Out(1, 2, "Authentication");
                Lcd_Out(2, 5, "Fails!");
                goto start;
        }

loop:
        //Now Authentication is Successfull Lets Try Something else
        //Lets Display the Data Coming from the USB HID Port to the LCD
        Lcd_Cmd(_LCD_CLEAR);
        Lcd_Out(1, 1, "Received Data:-");

loop_second:
        clear_buffer(Read_Buffer);
        //data_counter = 0;

        /*
        if(UART_Data_Ready()){
        while(1){
        Lcd_Cmd(_LCD_CLEAR);
        Lcd_Out(1,2,"Serial_jalan");
        writebuff[data_counter] = UART_Read();
        data_counter ++;
        if(data_counter > 63){
        break;
        }
        }
        }
        */

        /*if(data_counter > 10){
                data_counter = 0;
                }*/

        data_counter = 0;

        while (UART_Data_Ready()) {
                writebuff[data_counter] = UART_Read();
                delay_ms(5);
                data_counter++;
        }

        /*if (data_counter > 0) {
                Lcd_Cmd(_LCD_CLEAR);
                delay_ms(300);
                Lcd_out(1, 1, writebuff);
        }
        else{
        Lcd_Cmd(_LCD_CLEAR);
        delay_ms(300);
        Lcd_out(1,1,"no data");
        }*/



        if(Hid_Read()){
                while(!HID_Write(&writebuff,4));
        }


        goto loop_second;
}