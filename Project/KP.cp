#line 1 "D:/KAPE/Project/KP.c"
unsigned char Read_Buffer[16] absolute 0x500;
unsigned char Write_Buffer[16]absolute 0x510;
unsigned char recv[64] = "9989898";
int dataPointer = 0;
unsigned char num;
void interrupt() {
 USB_Interrupt_Proc();
 TMR0L = 100;
 INTCON.TMR0IF = 0;
}

sbit Lcd_RS at RC1_bit;
sbit Lcd_RW at RC0_bit;
sbit Lcd_EN at RC2_bit;
sbit Lcd_D7 at RD7_bit;
sbit Lcd_D6 at RD6_bit;
sbit Lcd_D5 at RD5_bit;
sbit Lcd_D4 at RD4_bit;
sbit Lcd_D3 at RD3_bit;
sbit Lcd_D2 at RD2_bit;
sbit Lcd_D1 at RD1_bit;
sbit Lcd_D0 at RD0_bit;
sbit Lcd_RS_Direction at TRISC1_bit;
sbit Lcd_RW_Direction at TRISC0_bit;
sbit Lcd_EN_Direction at TRISC2_bit;
sbit Lcd_D7_Direction at TRISD7_bit;
sbit Lcd_D6_Direction at TRISD6_bit;
sbit Lcd_D5_Direction at TRISD5_bit;
sbit Lcd_D4_Direction at TRISD4_bit;
sbit Lcd_D3_Direction at TRISD3_bit;
sbit Lcd_D2_Direction at TRISD2_bit;
sbit Lcd_D1_Direction at TRISD1_bit;
sbit Lcd_D0_Direction at TRISD0_bit;

char i;
void UART1_Write_Text_Newline(unsigned char msg[]) {
 UART1_Write_Text(msg);
 UART1_Write(10);
 UART1_Write(13);
}
void clear_buffer(unsigned char buffer[]) {
 unsigned int i = 0;
 while (buffer[i] != '\0') {
 buffer[i] = '\0';
 i++;
 }
}

void main() {
 UART1_Init(4800);
 Delay_ms(100);
 UART1_Write_Text("USB Test Program");
 ADCON1 |= 0x0F;
 CMCON |= 7;
 TRISB = 0x00;
 TRISC = 0x80;
 Lcd_Init();
 Delay_ms(100);
 Lcd_Cmd(_LCD_CLEAR);
 Delay_ms(100);
 Lcd_Cmd(_LCD_CURSOR_OFF);
 Delay_ms(100);
 Lcd_Out(1, 3, "PIC18F4550");
 Delay_ms(100);
 Lcd_Out(2, 3, "USB Example!");
 Delay_ms(200);
 INTCON = 0;
 INTCON2 = 0xF5;
 INTCON3 = 0xC0;
 RCON.IPEN = 0;
 PIE1 = 0;
 PIE2 = 0;
 PIR1 = 0;
 PIR2 = 0;






 T0CON = 0x47;
 TMR0L = 100;
 INTCON.TMR0IE = 1;
 T0CON.TMR0ON = 1;
 INTCON = 0xE0;



 UART1_Write(10);
 UART1_Write(13);
 UART1_Write_Text_Newline("Data is Ready to be Received from the PC");
 Hid_Enable(&Read_Buffer, &Write_Buffer);
 Delay_ms(200);

start:
 while (Hid_Read() == 0);

 if (Read_Buffer[0] == 0x81) {
 Lcd_Cmd(_LCD_CLEAR);
 Lcd_Out(1, 2, "Authentication");
 Lcd_Out(2, 8, "OK");
 while (!HID_Write(&recv, 64));
 }
 else {
 Lcd_Cmd(_LCD_CLEAR);
 Lcd_Out(1, 2, "Authentication");
 Lcd_Out(2, 5, "Fails!");
 goto start;
 }



loop_second:
 clear_buffer(recv);
 clear_buffer(Read_Buffer);

 while (1) {
 if (UART_Data_Ready()) {
 recv[dataPointer + 1] = UART_Read();
 dataPointer++;
 }

 if (dataPointer > 62)
 break;

 if (Hid_Read()) {
 recv[0] = (unsigned char) dataPointer;
 while (!HID_Write(&recv, 64));
 break;
 }
 }
 dataPointer = 0;
 Lcd_Cmd(_LCD_CLEAR);
 Lcd_Out(1, 1, "Received Data:-");
 Lcd_Out(2, 1, recv);

goto loop_second;
}
